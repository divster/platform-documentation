
# Glossary
## Solution
Solutions are used to group your application business logic. Solutions can contain entities, workflows, forms, reports, security roles and more.

Many different solutions can be deployed into one single instance of Platform. This enables seamless integration of across all your use cases.

Testing new doc version 1.1.1.1

# Business Rules
## Workflows
## Events

# Data
## Record Types
### Standard Properties

Id  Indicates something

CreatedOn  indicates something else
CreatedBy (One or Many User IDs)
DeletedOn
DeletedBy (One User ID)
ModifiedOn
ModifiedBy (One or Many User IDs)
Owner (User ID or Team ID)

## Records
## Virtual Records
## Audit Records
## Data Types
### Standard Data Types
Text
Date Time


# Security
## Security Roles
## Users
## Teams (User Group?)
## Business Unit (Hierarchical)
## Identity
## Federation

# UI

## Forms
## Reports


# Integration
## APIs Endpoints
## Data Endpoints
## Utilities (Imports, etc)